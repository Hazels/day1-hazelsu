# Daily Report (2023/07/10)
# O (Objective): 
## 1. What did we learn today?
### We learned PDCA cycle model,learning Pyramid,concept map and ORID method today.
## 2. What activities did you do? 
### I participated in ice-breaking operation and bulit a concept map with my team members.
## 3. What scenes have impressed you?
### I have been impressed by the TED video named 'build s school in cloud'.
---------------------

# R (Reflective): 
## Please use one word to express your feelings about today's class.
---------------------
### Interesting.

# I (Interpretive):
## What do you think about this? What was the most meaningful aspect of this activity?
---------------------
### I think the most meaningful aspect of this activity is brainstorm and building a concept with my team members,because it was also a good exercise for my presentation skillsI to present the result on behalf of our team to the class.I am also grateful to the team members for clarifying the logic of expression together with me so that I can better show the results of our joint efforts.



# D (Decisional): 
## Where do you most want to apply what you have learned today? What changes will you make?
---------------------
### I want to apply the ORID method to write my daily reports and use the PDCA model to plan my work.Compared to the past, I will express myself more confidently.


